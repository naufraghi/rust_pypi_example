=================
rust_pypi_example
=================


.. image:: https://img.shields.io/pypi/v/rust_pypi_example.svg
        :target: https://pypi.python.org/pypi/rust_pypi_example

.. image:: https://img.shields.io/travis/naufraghi/rust_pypi_example.svg
        :target: https://travis-ci.org/naufraghi/rust_pypi_example

.. image:: https://readthedocs.org/projects/rust-pypi-example/badge/?version=latest
        :target: https://rust-pypi-example.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/naufraghi/rust_pypi_example/shield.svg
     :target: https://pyup.io/repos/github/naufraghi/rust_pypi_example/
     :alt: Updates


Example of https://github.com/mckaymatt/cookiecutter-pypackage-rust-cross-platform-publish All the boilerplate for a Python Wheel package with a Rust binary module.


* Free software: MIT license
* Documentation: https://rust-pypi-example.readthedocs.io.


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `mckaymatt/cookiecutter-pypackage-rust-cross-platform-publish`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`mckaymatt/cookiecutter-pypackage-rust-cross-platform-publish`: https://github.com/mckaymatt/cookiecutter-pypackage-rust-cross-platform-publish

