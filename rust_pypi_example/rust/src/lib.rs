use std::os::raw::c_int;
use std::sync::{Arc, Mutex};
use std::thread;


#[derive(Default)]
pub struct Counter {
    counter: Arc<Mutex<i64>>,
    running: Arc<Mutex<bool>>,
    handle: Option<thread::JoinHandle<()>>,
}

impl Counter {
    pub fn new() -> Self {
        Counter {
            counter: Arc::new(Mutex::new(0)),
            running: Arc::new(Mutex::new(false)),
            handle: None,
        }
    }
    fn set_running(&self, running: bool) -> () {
        let mut run = self.running.lock().unwrap();
        *run = running;
    }
    pub fn start(&mut self) -> () {
        self.set_running(true);
        let counter = Arc::clone(&self.counter);
        let running = Arc::clone(&self.running);
        let handle = thread::spawn(move || {
            loop {
                if *running.lock().unwrap() != true {
                    break;
                }
                let mut num = counter.lock().unwrap();
                *num += 1;
            }
        });
        self.handle = Some(handle);
    }
    pub fn current(&self) -> i64 {
        *self.counter.lock().unwrap()
    }
    pub fn stop(self) -> Option<()> {
        self.set_running(false);
        if let Some(handle) = self.handle {
            handle.join().ok()
        } else {
            None
        }
    }
}


#[no_mangle]
pub extern "C" fn is_prime(n: *const c_int) -> c_int {
    let n = n as i32;
    if n < 2 {
        return 0;
    }
    for i in 2..n {
        if n % i == 0 {
            return 0;
        }
    }
    1
}


#[no_mangle]
pub extern "C" fn counter_new() -> *mut Counter {
    let counter = Box::new(Counter::new());
    Box::into_raw(counter)
}

#[no_mangle]
pub extern "C" fn counter_start(ptr: *mut Counter) {
    println!("Fire the counter");
    if ptr.is_null() {
        return;
    }
    let mut counter = unsafe {
        Box::from_raw(ptr)
    };
    counter.start();
    Box::into_raw(counter);
}

#[no_mangle]
pub extern "C" fn counter_current(ptr: *mut Counter) -> i64 {
    println!("Ask the counter");
    if ptr.is_null() {
        return 0;
    }
    let counter = unsafe {
        Box::from_raw(ptr)
    };
    let current = counter.current();
    Box::into_raw(counter);
    current
}

#[no_mangle]
pub extern "C" fn counter_free(ptr: *mut Counter) {
    println!("Freeing the counter");
    if ptr.is_null() {
        return;
    }
    let counter = unsafe {
        Box::from_raw(ptr)
    };
    counter.stop();
}


#[test]
fn test_is_prime() {
    let not_prime = 12;
    let is_a_prime = 13;
    let _true: c_int = 1;
    let _false: c_int = 0;
    assert_eq!(is_prime(not_prime as *const c_int), _false);
    assert_eq!(is_prime(is_a_prime as *const c_int), _true);
}
