
int is_prime(const int n);

typedef void* counter;

counter counter_new();
void counter_free(counter);
void counter_start(counter);
int counter_current(counter);
