#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_rust_pypi_example
----------------------------------

Tests for `rust_pypi_example` module.
"""
import time

import pytest


from rust_pypi_example import rust_pypi_example


@pytest.fixture
def response():
    """Sample pytest fixture.
    See more at: http://doc.pytest.org/en/latest/fixture.html
    """
    # import requests
    # return requests.get('https://github.com/audreyr/cookiecutter-pypackage')


def test_content(response):
    """Sample pytest test function with the pytest fixture as an argument.
    """
    # from bs4 import BeautifulSoup
    # assert 'GitHub' in BeautifulSoup(response.content).title.string


def test_rust_pypi_example():
    assert rust_pypi_example.rust_lib.is_prime(12) is 0
    assert rust_pypi_example.rust_lib.is_prime(13) is 1


def test_rust_pypi_counter():
    counter_prt = rust_pypi_example.rust_lib.counter_new()
    assert counter_prt is not None
    rust_pypi_example.rust_lib.counter_free(counter_prt)

def test_rust_pypi_counter_count():
    counter_prt = rust_pypi_example.rust_lib.counter_new()
    assert counter_prt is not None
    rust_pypi_example.rust_lib.counter_start(counter_prt)
    time.sleep(10)
    assert rust_pypi_example.rust_lib.counter_current(counter_prt) > 0
    rust_pypi_example.rust_lib.counter_free(counter_prt)
